//
//  New.swift
//  News
//
//  Created by Héctor Moreno on 02/04/18.
//  Copyright © 2018 Hkapp. All rights reserved.
//

import ObjectMapper
import RealmSwift

class New: Object, Mappable {
    
    dynamic var newsId: Int = 0
    dynamic  var title: String?
    dynamic  var newsDescription: String?
    dynamic  var image: String?
    dynamic  var thumbnail: String?
    dynamic var body: String?
    dynamic var date: String?
    dynamic var metaDesc: String?
    dynamic var videoLink: String?
    dynamic var permaLink: String?
    dynamic var category: Category!
    dynamic  var fileUrl: String?
    dynamic  var baseShareURL = "www.saludsonora.gob.mx/contenido/"
    dynamic var liked: Bool = false
    
    
    required convenience init?(map: Map) {
        self.init()
        
    }
    
    override static func primaryKey() -> String? {
        return "newsId"
    }
    
    func mapping(map: Map) {
        newsId              <- map["NewsId"]
        title               <- map["Title"]
        newsDescription         <- map["Description"]
        image               <- map["Image"]
        thumbnail           <- map["Thumbnail"]
        body                <- map["Body"]
        date                <- map["Date"]
        metaDesc            <- map["MetaDesc"]
        permaLink           <- map["Permalink"]
        category            <- map["Category"]
        fileUrl             <- map["FileUrl"]
    }
}
