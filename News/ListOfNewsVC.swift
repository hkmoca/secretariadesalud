//
//  ViewController.swift
//  News
//
//  Created by Héctor Moreno on 02/04/18.
//  Copyright © 2018 Hkapp. All rights reserved.
//

import UIKit
import RealmSwift


class ListOfNewsVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView?
    var news = [New]()
    lazy var filteredNews = [New]()
    let segueIdentifier = "showNewsDetails"
    var activityIndicatior = ActivityIndicator()
    var refreshControl = UIRefreshControl()
    let headerHeight: CGFloat = 78
    
    lazy var menuLauncher: MenuLauncherTV = {
        let launcher = MenuLauncherTV()
        launcher.listOfNews = self
        return launcher
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView?.dataSource = self
        tableView?.delegate = self
        activityIndicatior.startActivityIndicator(view)
        getTheNews()
        getCategories()
        setRefreshControls()
        print(Realm.Configuration.defaultConfiguration.fileURL!)
        
        navigationController?.navigationBar.barTintColor = UIColor.red
    }
    
    
    func getTheNews(){
        Model.getTheNews(completion: { (theNews) in
            self.news = theNews
            self.filteredNews = theNews
            self.tableView?.reloadData()
            if self.refreshControl.isRefreshing {
                self.refreshControl.endRefreshing()
            }
            self.activityIndicatior.stopActivityIndicator()
        }) { (NSError) in
            
        }
    }
    
    func getCategories(){
        Model.getTheCategories(completion: { (categories) in
            for category in categories{
                self.menuLauncher.categories.append(category.name!)
                print(category.name!)
            }
            
            
        }) { (NSError) in
            
        }
    }
    
    func setRefreshControls(){
        self.refreshControl.attributedTitle = NSAttributedString(string: "Desliza hacia abajo para actualizar")
        self.refreshControl.addTarget(self, action: #selector(ListOfNewsVC.refresh(_:)), for: UIControlEvents.valueChanged)
        self.tableView?.addSubview(refreshControl)
    }
    
    func refresh(_ sender:AnyObject){
        getTheNews()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == segueIdentifier {
            let indexPath = tableView?.indexPathForSelectedRow
            let descriptionNewVC = segue.destination as? NewsDescriptionVC
            descriptionNewVC?.newsInformation = filteredNews[(indexPath?.row)!]
        }
    }
    
    @IBAction func showCategoriesMenu(_ sender: Any) {
        menuLauncher.showCategoriesMenu()
        
    }
    
    func showFilteredNews(category: String){
        filteredNews = news.filter({($0.category.name?.localizedCaseInsensitiveContains("\(category)"))!})
        self.tableView?.reloadData()
    }
}

extension ListOfNewsVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredNews.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "newsCell", for: indexPath) as! NewsCell
        
        let showingNew = filteredNews[indexPath.row]
        cell.updateCellInformation(cellInfo: showingNew)
        return cell
    }
    

    override func viewWillAppear(_ animated: Bool) {
        if let index = self.tableView?.indexPathForSelectedRow{
            self.tableView?.deselectRow(at: index, animated: true)
        }
    }
}

