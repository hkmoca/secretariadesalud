//
//  ActivityIndicator.swift
//  News
//
//  Created by Héctor Moreno on 26/04/18.
//  Copyright © 2018 Hkapp. All rights reserved.
//

import UIKit

struct ActivityIndicator  {
    
     var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
     func startActivityIndicator(_ view: UIView){
        activityIndicator.center = view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        UIApplication.shared.beginIgnoringInteractionEvents()
        
    }
    
    func stopActivityIndicator(){
        activityIndicator.stopAnimating()
        UIApplication.shared.endIgnoringInteractionEvents()
    }
}

