//
//  MenuLauncher.swift
//  News
//
//  Created by Héctor Moreno on 19/04/18.
//  Copyright © 2018 Hkapp. All rights reserved.
//

import UIKit

class MenuLauncherTV: NSObject, UITableViewDelegate, UITableViewDataSource {
    
    var listOfNews: ListOfNewsVC?
    let blackView = UIView()
    let rowId = "categoryId"
    let rowHeight: CGFloat = 50
    var categories = [String]()
    var categoriesTableView = UITableView()
    
    
    override init(){
        super.init()
        categoriesTableView.dataSource = self
        categoriesTableView.delegate = self
        categoriesTableView.register(UITableViewCell.self, forCellReuseIdentifier: rowId)
        
    }
    
    func showCategoriesMenu() {
        
        if let window = UIApplication.shared.keyWindow {
            
            blackView.backgroundColor = UIColor(white: 0, alpha: 0.5)
            blackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dissmissWhenTapOnBLackView)))
            blackView.frame = window.frame
            blackView.alpha = 0
            
            window.addSubview(blackView)
            window.addSubview(categoriesTableView)
            
            let height: CGFloat = CGFloat(categories.count) * rowHeight
            let y = window.frame.height - height
            categoriesTableView.frame = CGRect(x: 0, y: window.frame.height, width: window.frame.width, height: height)
            
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.blackView.alpha = 1
                self.categoriesTableView.frame = CGRect(x: 0, y: y, width: self.categoriesTableView.frame.width, height: self.categoriesTableView.frame.height)
            }, completion: nil)
        }
    }
    
    
    func dissmissWhenTapOnBLackView(){
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            UIView.animate(withDuration: 0.5) {
                self.removeBlackView()
            }
        })
        
    }
    
    func refreshNewsTableView(selectedCategorie: String) {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            UIView.animate(withDuration: 0.5) {
                self.removeBlackView()
            }
        }) { (compleated: Bool) in
            
            self.listOfNews?.showFilteredNews(category: selectedCategorie)
        }
    }
    
    func removeBlackView(){
        self.blackView.alpha = 0
        
        if let window = UIApplication.shared.keyWindow {
            self.categoriesTableView.frame = CGRect(x: 0, y: window.frame.height, width: self.categoriesTableView.frame.width, height: self.categoriesTableView.frame.height)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: rowId, for: indexPath) 
        cell.textLabel?.text = categories[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return rowHeight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedCategorie = categories[indexPath.row]
        refreshNewsTableView(selectedCategorie: selectedCategorie)
        self.categoriesTableView.deselectRow(at: indexPath, animated: true)
    }
}
