//
//  FavoritesTableViewController.swift
//  Salud Sonora
//
//  Created by Héctor Moreno on 09/07/18.
//  Copyright © 2018 Hkapp. All rights reserved.
//

import UIKit
import RealmSwift
import Realm

class FavoritesTableViewController: UITableViewController {
    
    lazy var favorites = [New]()
    let segueIdentifier = "showNew"
    let realm = try! Realm()

    override func viewDidLoad() {
        super.viewDidLoad()
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        let favoriteNews = realm.objects(New.self).filter("liked = true")
        favorites += favoriteNews
        
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == segueIdentifier {
            let indexPath = tableView?.indexPathForSelectedRow
            let descriptionNewVC = segue.destination as? NewsDescriptionVC
            descriptionNewVC?.newsInformation = favorites[(indexPath?.row)!]
        }
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return favorites.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "favoriteNews", for: indexPath) as! FavoriteNewsTableViewCell
        let favoriteNewIndex = favorites[indexPath.row]
        cell.titleNew.text = favoriteNewIndex.title
        
        if let urlImage = URL(string: favoriteNewIndex.thumbnail!) {
            cell.imageNew?.af_setImage(withURL: urlImage)
        }

        return cell
    }

}
