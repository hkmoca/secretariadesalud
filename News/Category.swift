//
//  Categories.swift
//  News
//
//  Created by Héctor Moreno on 24/04/18.
//  Copyright © 2018 Hkapp. All rights reserved.
//

import ObjectMapper
import RealmSwift

class Category: Object, Mappable   {
    
    dynamic var categoryId: Int = 0
    dynamic var magazineId: Int = 0
    dynamic var name: String?
    dynamic var magazineTitle: String?
    dynamic var Permalink: String?
  
    required convenience init?(map: Map) {
        self.init()
        
    }
    
    override static func primaryKey() -> String? {
        return "categoryId"
    }
    
    func mapping(map: Map) {
        categoryId              <- map["CategoryId"]
        magazineId              <- map["MagazineId"]
        name                    <- map["Name"]
        magazineTitle           <- map["MagazineTitle"]
        Permalink               <- map["Permalink"]
 
    }
}

