//
//  FavoriteNewsTableViewCell.swift
//  Salud Sonora
//
//  Created by Héctor Moreno on 09/07/18.
//  Copyright © 2018 Hkapp. All rights reserved.
//

import UIKit

class FavoriteNewsTableViewCell: UITableViewCell {

    @IBOutlet weak var imageNew: UIImageView!
    @IBOutlet weak var titleNew: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
