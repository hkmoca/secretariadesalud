//
//  Router.swift
//  News
//
//  Created by Héctor Moreno on 18/04/18.
//  Copyright © 2018 Hkapp. All rights reserved.
//

import Foundation
import Alamofire

enum Router: URLRequestConvertible {
    
    
    static let baseURLString = "http://thecontent.mx"
    
    case getDataBase
    case getCategories
    
    var method: Alamofire.HTTPMethod {
        switch self {
        case .getDataBase, .getCategories:
            return .get
        }
    }
    
    var path: String {
        switch self {
        case .getDataBase:
            return "/api/20964385-859e-4711-bb9f-63ca81b64efe/News/GetNewsByMagazine"
        case .getCategories:
            return "/api/20964385-859e-4711-bb9f-63ca81b64efe/Categories/GetCategoriesByMagazine"
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = URL(string: Router.baseURLString)!
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue
        
        switch self {
        case .getDataBase, .getCategories:
            return try Alamofire.JSONEncoding.default.encode(urlRequest, with: nil)
            
        }
    }
}
