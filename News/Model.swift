//
//  Model.swift
//  News
//
//  Created by Héctor Moreno on 02/04/18.
//  Copyright © 2018 Hkapp. All rights reserved.
//


import ObjectMapper


class Model {
    
    static func getTheNews(completion: @escaping (_: [New]) -> Void, onFailure: @escaping (_: NSError) -> Void){
        Conection.getJSON(compleation: { (json) in
            let news: Array<New> = Mapper<New>().mapArray(JSONArray: json)
            print("Got the json into mapping Array")
            completion(news)
            
            
        }) { (NSError) in
            
        }
        
    }
    
    static func getTheCategories(completion: @escaping (_: [Category]) -> Void, on Failure: @escaping (_: NSError) -> Void){
            Conection.getCategories(compleation: { (json) in
                let categories: Array<Category> = Mapper<Category>().mapArray(JSONArray: json)
                print("Got the categories")
                completion(categories)
            }) { (NSError) in
                
        }
    }
}
