//
//  Conection.swift
//  News
//
//  Created by Héctor Moreno on 02/04/18.
//  Copyright © 2018 Hkapp. All rights reserved.
//


import Foundation
import Alamofire

class Conection {
    
    static func getJSON( compleation:  @escaping (_: [[String: AnyObject]]) -> Void, onFailure: @escaping (_: NSError) -> Void) {
        
        Alamofire.request(Router.getDataBase)
            .responseJSON { response in
                
                switch response.result {
                    
                case .success(let JSON):
                    if let json = JSON as?  [[String: AnyObject]]{
                        compleation(json)
                    } else {
                        print("Incorrect JSON Format")
                    }
                case .failure(let ERROR):
                    onFailure(ERROR as NSError)
                }
        }
    }
    
    static func getCategories( compleation: @escaping (_: [[String: AnyObject]]) -> Void, onFailure: @escaping (_: NSError) -> Void) {
        
        Alamofire.request(Router.getCategories)
            .responseJSON { response in
                switch response.result {
                case .success(let JSON):
                    if let json = JSON as?  [[String: AnyObject]]{
                        compleation(json)
                    } else {
                        print("Incorrect JSON Format")
                    }
                case .failure(let ERROR):
                    onFailure(ERROR as NSError)
                }
        }
    }
}

