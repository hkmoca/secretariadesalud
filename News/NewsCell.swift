//
//  NewsTVCell.swift
//  News
//
//  Created by Héctor Moreno on 02/04/18.
//  Copyright © 2018 Hkapp. All rights reserved.
//

import UIKit
import AlamofireImage

class NewsCell: UITableViewCell {
    
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var titleName: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var imageDisplay: UIImageView?
   
    override func awakeFromNib() {
        super.awakeFromNib()
    
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layoutIfNeeded()
        //cellView.backgroundColor = UIColor(red: 163/255.0, green: 125/255.0, blue: 125/255.0, alpha: 1.0)
        contentView.backgroundColor = UIColor(red: 217/255.0, green: 217/255.0, blue: 217/255.0, alpha: 1.0)
        cellView.layer.cornerRadius = 10
        cellView.layer.masksToBounds = false
        cellView.layer.shadowColor = UIColor.black.withAlphaComponent(0.2).cgColor
        cellView.layer.shadowOffset = CGSize(width: 0, height: 0)
        cellView.layer.shadowOpacity = 0.8
        
        imageDisplay!.layer.cornerRadius = 1
        imageDisplay!.clipsToBounds = true
        
        titleName.textColor = UIColor.gray
    
    }
    
    override func prepareForReuse() {
        
    }
        
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    func updateCellInformation(cellInfo: New){
        imageDisplay?.image = nil
        titleName.text = cellInfo.title
        date.text = cellInfo.date
        if let imageUrl = URL(string: cellInfo.thumbnail!){
            imageDisplay?.af_setImage(withURL: imageUrl)
        }
    }
    
}
