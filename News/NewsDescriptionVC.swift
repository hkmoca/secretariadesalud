//
//  NewsDescription.swift
//  News
//
//  Created by Héctor Moreno on 03/04/18.
//  Copyright © 2018 Hkapp. All rights reserved.
//


import UIKit
import RealmSwift

var MyObservationContext = 0

class NewsDescriptionVC: UIViewController {
    
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var thumbNail: UIImageView!
    @IBOutlet weak var webHeight: NSLayoutConstraint!
    @IBOutlet weak var newsTitle: UILabel!
    @IBOutlet weak var favoriteButton: UIBarButtonItem!
    let realm = try! Realm()
    let filledStar = UIImage(named: "icons8-star-filled-30") as UIImage?
    let emptyStar = UIImage(named: "icons8-add-to-favorites-30") as UIImage?
    
    var newsInformation: New!
    var observing = false
    var activityIndicatior = ActivityIndicator()
    var pressed: Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor(red: 217/255.0, green: 217/255.0, blue: 217/255.0, alpha: 1.0)
        setWebView()
        setUpperThumnailImage()
        setUpNewsTitle()
        setUpFavoriteButton()
        
    }
    
    func setUpFavoriteButton(){
        let favorite: Bool?
        let isNewFavorited = realm.objects(New.self).filter("newsId = \(newsInformation.newsId)")
        if !isNewFavorited.isEmpty {
            favorite = isNewFavorited[0].liked
        } else {
            favorite = false
        }
        
        if (favorite)! {
            favoriteButton.image = filledStar
            pressed = true
        } else {
            favoriteButton.image = emptyStar
            pressed = false
        }
    }
    
    @IBAction func saveToFavorites(_ sender: Any) {
        
        if !(pressed)!  {
            favoriteButton.image = filledStar
            pressed = true
            try! self.realm.write {
                newsInformation.liked = true
                realm.add(newsInformation, update: true)
            }
        } else {
            favoriteButton.image = emptyStar
            pressed = false
            
            try! self.realm.write {
                newsInformation.liked = false
                realm.add(newsInformation, update: true)
            }
        }
    }
    
    func setWebView(){
        webView.layer.cornerRadius = 10
        webView.clipsToBounds = true
        webView.scrollView.isScrollEnabled = false
        webView.delegate = self
        
        thumbNail.layer.cornerRadius = 10
        thumbNail.clipsToBounds = true
        activityIndicatior.startActivityIndicator(view)
        
        if newsInformation.category.Permalink == "noticias" || newsInformation.category.Permalink == "audio" || newsInformation.category.Permalink == "programas" || newsInformation.category.Permalink == "menu-superior" || newsInformation.category.Permalink == "video" {
            if let htmlString = newsInformation.body {
                webView.loadHTMLString(htmlString, baseURL: nil)
            }
        } else if newsInformation.category.Permalink == "licitaciones" {
            if  let htmlString = newsInformation.fileUrl {
                let htmlFileurl = URL(string: htmlString)
                webView.loadRequest(URLRequest(url: htmlFileurl!))
            }
        }
    }
    
    func setUpperThumnailImage(){
        if let urlImage = URL(string: newsInformation.thumbnail!) {
            thumbNail?.af_setImage(withURL: urlImage)
        }
    }
    
    func setUpNewsTitle(){
        if let title = newsInformation.title {
            newsTitle.text = title
        }
    }
    
    @IBAction func shareLink(_ sender: Any) {
        let activityVC = UIActivityViewController(activityItems: ["\(newsInformation.baseShareURL)\(newsInformation.newsId)/\(newsInformation.permaLink!)"], applicationActivities: nil)
        activityVC.popoverPresentationController?.sourceView = self.view
        
        self.present(activityVC, animated: true, completion: nil)
    }
    
    deinit {
        stopObservingHeight()
    }
    
    func startObservingHeight() {
        let options = NSKeyValueObservingOptions([.new])
        webView.scrollView.addObserver(self, forKeyPath: "contentSize", options: options, context: &MyObservationContext)
        observing = true;
    }
    
    func stopObservingHeight() {
        webView.scrollView.removeObserver(self, forKeyPath: "contentSize", context: &MyObservationContext)
        observing = false
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        guard let keyPath = keyPath else {
            super.observeValue(forKeyPath: nil, of: object, change: change, context: context)
            return
        }
        switch keyPath {
        case "contentSize":
            if context == &MyObservationContext {
                webHeight.constant = webView.scrollView.contentSize.height
            }
        default:
            super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
        }
    }
}

extension NewsDescriptionVC: UIWebViewDelegate {
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.activityIndicatior.stopActivityIndicator()
        webHeight.constant = webView.scrollView.contentSize.height
        if (!observing) {
            startObservingHeight()
        }
    }
}






